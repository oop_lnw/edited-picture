              IDENTIFICATION DIVISION.
       PROGRAM-ID. Edit3.
       AUTHOR. Ton.
       ENVIRONMENT DIVISION.
       CONFIGURATION SECTION.
       SPECIAL-NAMES.
           CURRENCY SIGN IS "£".
           CURRENCY SIGN IS "$".
           CURRENCY SIGN IS "¥".
      * CURRENCY SIGN IS "GBP" WITH PICTURE SYMBOL "£"
      * CURRENCY SIGN IS "USD" WITH PICTURE SYMBOL "$"
      * CURRENCY SIGN IS "JPY" WITH PICTURE SYMBOL "¥".

       DATA DIVISION.
       WORKING-STORAGE SECTION.
       01 DollarValue        PIC 9999V99.

       01 PrnDollarValue     PIC $$$,$$9.99.
       01 PrnYenValue        PIC ¥¥¥,¥¥9.99.
       01 PrnPoundValue      PIC £££,££9.99.

       01 DollarToPoundRate   PIC 99V9(6) VALUE 0.640138.
       01 DollarToYenRate     PIC 99V9(6) VALUE 98.6600.

       PROCEDURE DIVISION.
       Begin.
           DISPLAY "Enter a dollar value to convert :- " 
           WITH NO ADVANCING ACCEPT DollarValue
           MOVE DollarValue TO PrnDollarValue

           COMPUTE PrnYen-Value ROUNDED = 
           DollarValue * DollarToYenRate

           COMPUTE PrnPoundValue ROUNDED = 
           DollarValue * DollarToPoundRate

           DISPLAY "Dollar value = "  PrnDollarValue
           DISPLAY "Yen value    = "  PrnYenalue
           DISPLAY "Pound value  = "  PrnPoundValue
       GOBACK.